package io.dnadev.incubationchannels.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import io.dnadev.incubationchannels.R;
import io.dnadev.incubationchannels.activity.MainActivity;
import io.dnadev.incubationchannels.fragment.BottomSheetDialogs;
import io.dnadev.incubationchannels.model.Channel;

/**
 * Created by rahulmuthyam on 13/03/18.
 * RecyclerViewAdapter used to render Channel items
 */

public class ChannelListAdapter extends RecyclerView.Adapter<ChannelViewHolder> {
    private MainActivity activity;
    private ArrayList<Channel> channels;
    private String defaultName;

    public ChannelListAdapter(MainActivity activity, String defaultName) {
        this.activity = activity;
        this.defaultName = defaultName;
        this.channels = new ArrayList<>();
    }

    public ChannelListAdapter(MainActivity activity, ArrayList<Channel> channels, String defaultName) {
//        int spos = channels.size()>0 ? channels.get(0).getPos() : -1;
//        int epos = channels.size()>0 ? channels.get(channels.size() - 1).getPos() : -1;
//        Log.wtf("spos : " + spos, "epos : " + epos);

        this.activity = activity;
        this.defaultName = defaultName;
        this.channels = channels;
    }


    @NonNull @Override
    public ChannelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChannelViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_channel, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChannelViewHolder holder, int position) {
        if (channels.size() == 0) {
            Glide.with(holder.root).load(R.drawable.default_channel).apply(RequestOptions.centerInsideTransform().error(R.drawable.default_channel)).into(holder.iv_pic);
            holder.tv_name.setText("No " + defaultName + " available...");
            holder.tv_type.setText("");
            holder.root.setOnClickListener(null);
            return;
        }
        final Channel channel = channels.get(position);
        Glide.with(holder.root).load(channel.getIconURL()).apply(RequestOptions.centerInsideTransform().error(R.drawable.default_channel)).into(holder.iv_pic);
        holder.tv_name.setText(channel.getPos() + " : " + channel.getName());
        holder.tv_type.setText(channel.getType());
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomSheetDialogs(activity).viewChannelInfo(channel).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return channels.size() == 0 ? 1 : channels.size();
    }

    public int getRealItemCount() {
        return channels.size();
    }


    public void setChannels(ArrayList<Channel> channels) {
        this.channels = channels;
    }

    public void addChannelsFirst(ArrayList<Channel> channels) {
        this.channels.addAll(0, channels);
    }

    public void addChannelsLast(ArrayList<Channel> channels) {
        this.channels.addAll(channels);
    }


    public int getChannelIndexAt(int pos) {
        if(channels.size()==0) return -1;
        return channels.get(pos).getPos();
    }

    public int getFirstChannelIndex() {
        if(channels.size()>0) return channels.get(0).getPos();
        return -1;
    }

    public int getLastChannelIndex() {
        if(channels.size()>0) return channels.get(getRealItemCount()-1).getPos();
        return -1;
    }
}
