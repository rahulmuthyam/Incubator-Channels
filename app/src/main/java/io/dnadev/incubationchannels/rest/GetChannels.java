package io.dnadev.incubationchannels.rest;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.dnadev.incubationchannels.model.Channel;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by rahulmuthyam on 13/03/18.
 */

public class GetChannels extends AsyncTask<Void, Void, ArrayList<Channel>> {
    private Callback callback;

    private int responseCode = -1;
    private String responseMessage = "Unknown";

    public GetChannels(Callback callback) {
        this.callback = callback;
    }

    @Override
    public ArrayList<Channel> doInBackground(Void... d) {
        try {
            OkHttpClient okHttpClient = new OkHttpClient
                    .Builder()
                    .connectTimeout(15000, TimeUnit.MILLISECONDS)
                    .build();

            Request request = new Request.Builder()
                    .url(URL.CHANNELS)
                    .get()
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            responseCode = response.code();
            responseMessage = response.message();

            switch (responseCode) {
                case 200:
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray jsonArray = new JSONArray(jsonObject.getString("channels"));
                    ArrayList<Channel> channels = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject json = jsonArray.getJSONObject(i);
                        Channel channel = new Channel(
                                i,
                                json.optString("iconUrl", "Unknown").trim(),
                                json.optString("channelName", "Unknown").trim(),
                                json.optString("description", "Unknown").trim(),
                                json.optString("category", "Unknown").trim(),
                                json.optString("creatorUserName", "Unknown").trim());
                        if (channel.getDescription().trim().length() == 0)
                            channel.setDescription("Unknown");
                        channels.add(channel);
                    }

                    return channels;
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseCode = -1;
            responseMessage = e.getMessage();
        }
        return null;
    }

    @Override
    public void onPostExecute(ArrayList<Channel> channels) {
        if (responseCode == 200) callback.onFinish(channels);
        else callback.onError(responseCode, responseMessage);
    }

    public interface Callback {
        void onFinish(ArrayList<Channel> channels);
        void onError(int code, String message);
    }
}
