package io.dnadev.incubationchannels.model;

/**
 * Created by rahulmuthyam on 13/03/18.
 * Class used to hold information regarding a Channel
 */

public class Channel {
    private String iconURL;
    private String name;
    private String description;
    private String type;
    private String creator;
    private boolean favourite = false;
    private int pos = -1;

    public Channel(int pos, String iconURL, String name, String description, String type, String creator) {
        this.pos = pos;
        this.iconURL = iconURL;
        this.name = name;
        this.description = description;
        this.type = type;
        this.creator = creator;
    }


    public int getPos() {
        return pos;
    }

    public String getCreator() {
        return creator;
    }

    public String getDescription() {
        return description;
    }

    public String getIconURL() {
        return iconURL;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public boolean isFavourite() {
        return favourite;
    }


    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }
}
